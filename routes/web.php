<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Halaman awal pasien
Route::get('/','HomeController@index');

#Register, Login dan Logout
Auth::routes();
Route::get('/register_user', function () { return view('auth.register');})->name('Reg_user');
Route::get('/register_admin', function () { return view('auth.register_admin');})->name('Reg_Admin');
Route::post('kirim_register', 'Auth\LoginController@register')->name('kirim_register');

//=============== Grub User ADMIN =============== //
Route::group(['namespace' => 'Admin', 'middleware' => ['auth_admin'], 'prefix' => 'admin'], function () {

    Route::get('/landing','adminController@index')->name('landing');

    #Pasien
    Route::get('/pasien','pasienController@getPasien')->name('pasien');
    Route::get('/pasien/{id}', 'pasienController@hapus_pasien')->name('hapus_pasien');

    #History
    Route::get('/history/history','historyController@getHistory')->name('history');
    Route::get('/history/history/{id}','historyController@hapus_history')->name('hapus_history');

    #History Gejala
    Route::get('/history/history_gejala','hGejalaController@getHGejala')->name('hgejala');
    Route::get('/history/history_gejala/{id}','hGejalaController@hapus_HGejala')->name('hapus_hgejala');    

    #Mapping
    Route::get('/master/mapping','mappingController@getMapping')->name('mapping');
    Route::get('/master/mapping/{id}','mappingController@hapus_mapping')->name('hapus_mapping');
    Route::post('/master/mapping/tambah','mappingController@simpan_mapping')->name('simpan_mapping');

    #Gejala
    Route::get('/master/gejala','gejalaController@getGejala')->name('gejala');
    Route::get('/master/gejala/{id}','gejalaController@hapus_gejala')->name('hapus_gejala');
    Route::post('/master/gejala/tambah','gejalaController@simpan_gejala')->name('simpan_gejala');

    #Penyakit
    Route::get('/master/penyakit','penyakitController@getPenyakit')->name('penyakit');
    Route::get('/master/penyakit/{id}','penyakitController@hapus_penyakit')->name('hapus_penyakit');
    Route::post('/master/penyakit/tambah','penyakitController@simpan_penyakit')->name('simpan_penyakit');    

});

//=============== Grub User Pasien =============== //
Route::group(['namespace' => 'User', 'prefix' => 'User'], function () {
    Route::get('/biodata','userController@index')->name('biodata');
    Route::get('/landing','userController@userhal')->name('userhal');
    Route::get('/soal','userController@getSoal')->name('soal');
    Route::post('/send-biodata','userController@postBiodata')->name('postBiodata');
    Route::post('/postJawaban','userController@postJawaban')->name('postJawaban');
    Route::get('/soal/{id_history}','userController@getHasil')->name('getHasil');
    Route::get('/soal/perawatan/{id_penyakit}','userController@getPerawatan')->name('getPerawatan');
    Route::get('info','infoCovController@indexCov' );

});

//Change password
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');