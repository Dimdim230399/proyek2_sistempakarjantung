<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;


class ApiHelper
{

    public static function  loadfile()
    {
        $file = new Client(['verify' => public_path('ssl/cacert.pem')]);
        return $file;
    }

    public static function Token()
    {
        $data_session = Session::get('user_data');
        $token = $data_session['token'];
        $myHeader = array(
            "token" => $token,
        );
        return $myHeader;
    }

    public static function m_gejala()
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url = "http://127.0.0.1/api_covid/public/getGejala";
        $response = $client->get($url,['headers'=>$token]);#Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET
        return $data['result'];
    }

    public static function m_penyakit()
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url = "http://127.0.0.1/api_covid/public/getPenyakit";
        $response = $client->get($url,['headers'=>$token]);#Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET
        return $data['result'];
    }

    public static function m_kota()
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url = "http://127.0.0.1/api_covid/public/getKota";
        $response = $client->get($url,['headers'=>$token]);#Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET
        return $data['result'];
    }

    public static function format_tanggal($tgl)
    {

        $bln = date("m", strtotime($tgl));
        $bln_h = array('01' => "Januari", "02" => "Februari", "03" => "Maret", "04" => "April", "05" => "Mei", "06" => "Juni", "07" => "Juli", "08" => "Agustus", "09" => "September", "10" => "Oktober", "11" => "Nopember", "12" => "Desember");
        $bln = $bln_h[$bln];
        $tg = date("d", strtotime($tgl));
        $thn = date("Y", strtotime($tgl));
        $print = $tg.' '.$bln.' '.$thn;

        return $print;
    }
}
