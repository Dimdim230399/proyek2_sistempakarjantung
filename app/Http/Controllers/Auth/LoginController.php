<?php
  
namespace App\Http\Controllers\Auth;
   
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   
    private function validate_input($request, $id = null)
	{

		return Validator::make(
			$request->all(),
			[
				'username' => 'required',
				'password' => 'required',
			]
		);
    }

    public function login(Request $request)
    {   
        
        if ($request->isMethod('get')) {
			return redirect('login');
		}

		$validation = $this->validate_input($request);

		if ($validation->fails()) {
			return 'ok';
		}
		
		$username = $request->username;
		$password = $request->password;

		$client = new Client(['verify' => public_path('ssl/cacert.pem')]);

		$myBody = array(
			"username" => $username,
			"password" => $password,
        );
        
        $params['form_params'] = $myBody;  

        // return $params;
		$url = "127.0.0.1/api_covid/public/login";
        
		try {
			$request = $client->post($url,$params);
            $login = \GuzzleHttp\json_decode($request->getBody(), true);					
		} catch (RequestException $e) {			
			return redirect('login')->withErrors('Username atau Password Salah')->withInput();
			// You can check for whatever error status code you need 		
		} catch (\Exception $e) {		
			// There was another exception.
		}

        if (isset($login['result'])) {
            
            $data = $login['result'];

            if ($data != null) {
                    $data_user = [];
                    $data_user['userid'] = $data['id'];
                    $data_user['username'] = $data['username'];
                    $data_user['password'] = $data['password'];
                    $data_user['name'] = $data['name'];
                    $data_user['level'] = $data['level'];
                    $data_user['email'] = $data['email'];
                    $data_user['token'] = $data['token'];
                    session::put('user_data', $data_user);

                    // Setting landing page per user
                    if ($data_user['level'] == 'admin') {
                        return redirect('admin/master/gejala');
                    } elseif ($data_user['level'] == 'user') {
                        return redirect('User/landing');
                    }
			
			} else {
                // dd('eror');
				return redirect('login')->withErrors('Username Belum Diatur')->withInput();
			}
		} else {
            // dd('eror');
			return redirect('login')->withErrors('Username atau Password Salah')->withInput();
		}
    }

    public function logout()
	{
		session_start();

		$user = session::get('user_data');
		$id = trim($user['userid']);
		
		$client = new Client(['verify' => public_path('ssl/cacert.pem')]);
		$myBody = array(
			"id" => $id,
		);
		$url = "localhost/api_covid/public/logout";
		try {
			$request = $client->post($url,['form_params'=>$myBody]);
			$login = \GuzzleHttp\json_decode($request->getBody(), true);
			// Here the code for successful request
		
		} catch (RequestException $e) {
			// if ($e->getResponse()->getStatusCode() == '400') {
			// 		echo "Got response 400";
			// }
			$login = array('pesan'=>'error');
			// You can check for whatever error status code you need 
		} catch (\Exception $e) {
			// There was another exception.
		}

		Session::flush();

		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach ($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name  = trim($parts[0]);
				setcookie($name, '', time() - 1000);
				setcookie($name, '', time() - 1000, '/');
			}
		}

		$_SESSION = array();

		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(
				session_name(),
				'',
				time() - 42000,
				$params["path"],
				$params["domain"],
				$params["secure"],
				$params["httponly"]
			);
		}

		session_destroy();

		return redirect('/admin/landing');
    }    
    

#=========================================== Register ===========================================
    protected $pesan = array(
        'name.required' => 'Nama wajib diisi', 
        'username.required' => 'Username wajib diisi',
        'email.required' => 'Email wajib diisi',
        'password.required' => 'Password wajib diisi');    

    protected $aturan = array(
        'name'     =>  'required|string|max:50',
        'username' =>  'required|string|max:20|unique:users',
        'email'    =>  'required|string|email|unique:users',
        'password' =>  'required|string|min:6|confirmed'
    );

    public function register(Request $request)
    {
		$client = new Client(['verify' => public_path('ssl/cacert.pem')]);
		$myBody = array(
			"username" => $request->username,
			"name" => $request->name,
			"email" => $request->email,
			"password" => $request->password,
			"level" => $request->level,
        );
        
        $params['form_params'] = $myBody;  

        
		$url = "localhost/api_covid/public/register";
        $request = $client->post($url,$params);
        $login = \GuzzleHttp\json_decode($request->getBody(), true);
        
		try {
			// Here the code for successful request
		
		} catch (RequestException $e) {
			
			return redirect()->route('Reg_user')->with($this->pesan);
			// You can check for whatever error status code you need 
		
		} catch (\Exception $e) {		
			// There was another exception.
        }
        
        return redirect('login');
    }
}