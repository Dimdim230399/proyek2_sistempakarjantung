<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     // $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $request = collect( Http::get('https://covid19.keponet.com/api/provinsi/')->json());
        $request2 = collect( Http::get('https://covid19.keponet.com/api/negara/')->json());
        $request3 = collect( Http::get('http://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=8a77f5f25eae40668093f1dab97a80a0')->json());
        $data  = $request->flatten(1);
        $data1 = $request2->flatten(1);
        // $data2 = $request3->flatten(2);

        $labels = $data->pluck('Provinsi');
        $positif = $data->pluck('Kasus_Posi');
        $recov = $data->pluck('Kasus_Semb');
        $deat = $data->pluck('Kasus_Meni');

        // $chart = new CovidChart;
        // // $chart->labels($labels);
        // // $chart->dataset('Data Kasus Positif Covid-19' , 'pie', $positif);
        //  dd($request3);
        return view('index_user',compact('data1','labels','positif','recov','deat','request3'));

    }
}
