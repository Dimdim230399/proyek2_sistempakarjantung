<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class pasienController extends Controller
{

    private $Client;
    private $token;
    private $myHeader;

    public function cek()
    {
        $data_session = Session::get('user_data');
        $this->token = $data_session['token'];
        $this->Client = new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "token" => $this->token,
        );
    }

    public function getPasien(Request $request)
    {     
            
        if ($request->ajax()) {
            $this->cek();                

            $url = "http://127.0.0.1/api_covid/public/getPasien/all";    
            $response = $this->Client->get($url, ['headers' => $this->myHeader]);
            $pasien = \GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET 
            
            // $dataPasien = $pasien;                        
                                    
            return DataTables::of($pasien)
                ->addColumn('action', function($data){                     
                    $button = '<a href="#" class="btn btn-xs btn-danger
                    btnDelete" data-id="'.$data['id'].'"><i class="fas fa-trash"></i></a> ';                                                           
                
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);                
        }        
        return view('admin.pasien');
    }  
    
    public function hapus_pasien($id)
    {        
        $this->cek();
        
        $url ="http://127.0.0.1/api_covid/public/postDPasien";     
        
        $params['headers'] = $this->myHeader;
        $params['form_params'] = [            
            'id' => $id,
        ];           

        $response = $this->Client->post($url, $params);        
        $pasien = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return redirect($_SERVER['HTTP_REFERER']);
    }
}    