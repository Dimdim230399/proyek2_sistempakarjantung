<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use ApiHelper;
use DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class dinkesController extends Controller
{
    
    public function getDinkes(Request $request)
    {     
        $m_kota = ApiHelper::m_kota();                 

        if ($request->ajax()) {
            $client = ApiHelper::loadfile();
            $token = ApiHelper::Token();   
                        
            $url = "http://127.0.0.1/api_covid/public/getDinkes";    
            $response = $client->get($url, ['headers' => $token]);
            $DataDinkes = \GuzzleHttp\json_decode($response->getBody(), true);
            
            $dinkes = $DataDinkes['result'];

            return DataTables::of($dinkes)
                ->addColumn('action', function($data){                                         
                    $button ='<a href="javascript:void(0)" class="btn btn-xs btn-warning btnEdit"
                    data-id="'.$data['id'].'" data-id_kota="'.$data['id_kota'].'"
                    data-nama_dinkes="'.$data['nama_dinkes'].'" data-alamat_dinkes="'.$data['alamat_dinkes'].'"
                    data-tlp_dinkes="'.$data['tlp_dinkes'].'"><i class="far fa-edit"></i></a>&nbsp&nbsp';                    
                    $button = $button.'<a href="javascript:void(0)" class="btn btn-xs btn-danger
                    btnDelete" data-id="'.$data['id'].'"><i class="far fa-trash-alt"></i></a> ';
                    
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);                
        }        

        $data['m_kota']= $m_kota;         

        return view('admin.dinkes', $data);
    }  
    
    public function hapus_dinkes($id)
    {                     
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url ="http://127.0.0.1/api_covid/public/postDDinkes";     
        
        $params['headers'] = $token;
        $params['form_params'] = [            
            'id' => $id,
        ];           

        $response = $client->post($url, $params);        
        $dinkes = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function simpan_dinkes(Request $request)
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();
     
        $data_session = Session::get('user_data'); 

        $id = $request->id;

        if($id == NULL){
            $data_array = array(
                'created_by' => $data_session['name'] 
            );
        } else {
            $data_array = array(
                'updated_by' => $data_session['name'] 
            );         
        }

        $data = array_merge($request->except('_token'), $data_array); 
   
        $url = "http://127.0.0.1/api_covid/public/postDinkes";    

        $params['headers'] = $token;
        $params['form_params'] = $data;
        
        $response = $client->post($url, $params);
        $dinkes = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return response()->json($dinkes);        
    }
}    