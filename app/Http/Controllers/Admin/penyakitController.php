<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use DataTables;
use ApiHelper;

class penyakitController extends Controller
{    

    public function getPenyakit(Request $request){ 
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url = "http://127.0.0.1/api_covid/public/getPenyakit";    

        $respon = $client->get($url, ['headers' => $token]);
        $datafinal =\GuzzleHttp\json_decode($respon->getBody(), true);    
        $hasil= $datafinal['result'];

        if ($request->ajax()){
            return DataTables::of($hasil)
                ->addColumn('action', function($data){
                    $button ='<a class="btn btn-xs btn-warning editData"
                    data-id="'.$data['id'].'" data-nama_penyakit="'.$data['nama_penyakit'].'" 
                    href="javascript:void(0)"><i class="far fa-edit"></i></a>&nbsp&nbsp';
                    $button = $button.'<a class="btn btn-xs btn-danger deleteData" 
                    data-id="'.$data['id'].'" href="javascript:void(0)"><i class="far fa-trash-alt"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.penyakit');
    }


    public function simpan_penyakit(Request $request)
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();         

        $data_session = Session::get('user_data');    

        $id = $request->id;

        if ($id == null){
            $data_array = array(
                'created_by' => $data_session['name'] 
            );
        } else {
            $data_array = array(
                'updated_by' => $data_session['name'] 
            );         
        }
        
        $data_kirim = array_merge($request->except('_token'), $data_array);   

        $url = "http://127.0.0.1/api_covid/public/postPenyakit"; 

        $params['headers'] = $token;
        $params['form_params'] = $data_kirim;
        
        $response = $client->post($url, $params);
        $dataGejala = \GuzzleHttp\json_decode($response->getBody(), true);

        return response()->json($dataGejala);

    }

   public function hapus_penyakit($id){
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url = 'http://127.0.0.1/api_covid/public/postDPenyakit';

        $params['headers'] = $token;
        $params['form_params'] = [
            'id' => $id,
        ];   

        $response = $client->post($url, $params);

        $penghargaan=\GuzzleHttp\json_decode($response->getBody(), true);
        
        return response()->json(['success'=>'Data Deleted successfully!']);
    }

}