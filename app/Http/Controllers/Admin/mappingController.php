<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use ApiHelper;
use DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class mappingController extends Controller
{
    
    public function getMapping(Request $request)
    {     
        $m_gejala = ApiHelper::m_gejala();
        $m_penyakit = ApiHelper::m_penyakit();         

        if ($request->ajax()) {
            $client = ApiHelper::loadfile();
            $token = ApiHelper::Token();   
                        
            $url = "http://127.0.0.1/api_covid/public/getMapping";    
            $response = $client->get($url, ['headers' => $token]);
            $DataMapping = \GuzzleHttp\json_decode($response->getBody(), true);
            
            $mapping = $DataMapping['result'];

            return DataTables::of($mapping)
                ->addColumn('action', function($data){                                         
                    $button ='<a href="javascript:void(0)" class="btn btn-xs btn-warning btnEdit"
                    data-id="'.$data['id'].'" data-id_penyakit="'.$data['id_penyakit'].'"
                    data-id_gejala="'.$data['id_gejala'].'"><i class="far fa-edit"></i></a>&nbsp&nbsp';                    
                    $button = $button.'<a href="javascript:void(0)" class="btn btn-xs btn-danger
                    btnDelete" data-id="'.$data['id'].'"><i class="far fa-trash-alt"></i></a> ';
                    
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);                
        }        

        $data['m_gejala']= $m_gejala; 
        $data['m_penyakit']= $m_penyakit; 

        return view('admin.mapping', $data);
    }  
    
    public function hapus_mapping($id)
    {                     
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url ="http://127.0.0.1/api_covid/public/postDMapping";     
        
        $params['headers'] = $token;
        $params['form_params'] = [            
            'id' => $id,
        ];           

        $response = $client->post($url, $params);        
        $mapping = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function simpan_mapping(Request $request)
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();
     
        $data_session = Session::get('user_data'); 

        $id = $request->id;

        if($id == NULL){
            $data_array = array(
                'created_by' => $data_session['name'] 
            );
        } else {
            $data_array = array(
                'updated_by' => $data_session['name'] 
            );         
        }

        $data = array_merge($request->except('_token'), $data_array); 
   
        $url = "http://127.0.0.1/api_covid/public/postMapping";    

        $params['headers'] = $token;
        $params['form_params'] = $data;
        
        $response = $client->post($url, $params);
        $mapping = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return response()->json($mapping);        
    }
}    