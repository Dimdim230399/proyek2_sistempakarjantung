<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use ApiHelper;
use DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class kotaController extends Controller
{
    
    public function getKota(Request $request)
    {                     

        if ($request->ajax()) {
            $client = ApiHelper::loadfile();
            $token = ApiHelper::Token();   
                        
            $url = "http://127.0.0.1/api_covid/public/getKota";    
            $response = $client->get($url, ['headers' => $token]);
            $dataKota = \GuzzleHttp\json_decode($response->getBody(), true);
            
            $kota = $dataKota['result'];

            return DataTables::of($kota)
                ->addColumn('action', function($data){                                         
                    $button ='<a href="javascript:void(0)" class="btn btn-xs btn-warning btnEdit"
                    data-id="'.$data['id'].'" data-nama_kota="'.$data['nama_kota'].'">
                    <i class="far fa-edit"></i></a>&nbsp&nbsp';                    
                    $button = $button.'<a href="javascript:void(0)" class="btn btn-xs btn-danger
                    btnDelete" data-id="'.$data['id'].'"><i class="far fa-trash-alt"></i></a> ';
                    
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);                
        }

        return view('admin.kota');
    }  
    
    public function hapus_kota($id)
    {                     
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $url ="http://127.0.0.1/api_covid/public/postDKota";     
        
        $params['headers'] = $token;
        $params['form_params'] = [            
            'id' => $id,
        ];           

        $response = $client->post($url, $params);        
        $kota = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function simpan_kota(Request $request)
    {
        $client = ApiHelper::loadfile();
        $token = ApiHelper::Token();

        $data_session = Session::get('user_data'); 

        $id = $request->id;

        if($id == NULL){
            $data_array = array(
                'created_by' => $data_session['name'] 
            );
        } else {
            $data_array = array(
                'updated_by' => $data_session['name'] 
            );         
        }

        $data = array_merge($request->except('_token'), $data_array); 

        $url = "http://127.0.0.1/api_covid/public/postKota";    

        $params['headers'] = $token;
        $params['form_params'] = $data;
        
        $response = $client->post($url, $params);
        $kota = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return response()->json($kota);        
    }
}    