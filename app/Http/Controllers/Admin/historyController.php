<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class historyController extends Controller
{

    private $Client;
    private $token;
    private $myHeader;

    public function cek()
    {
        $data_session = Session::get('user_data');
        $this->token = $data_session['token'];
        $this->Client = new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "token" => $this->token,
        );
    }

    public function getHistory(Request $request)
    {     
            
        if ($request->ajax()) {
            $this->cek();                

            $url = "http://127.0.0.1/api_covid/public/getHistory/all";    
            $response = $this->Client->get($url, ['headers' => $this->myHeader]);
            $history = \GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET 
            
            $dataHistory = $history;            
            
            return DataTables::of($dataHistory)
                ->addColumn('action', function($data){                     
                    $button = '<a href="javascript:void(0)" class="btn btn-xs btn-danger
                    btnDelete" data-id="'.$data['id'].'"><i class="fas fa-trash"></i></a> ';                   
                
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);                
                
        }        
        return view('admin.history');
    }
    
    public function hapus_history($id)
    {        
        $this->cek();
        
        $url ="http://127.0.0.1/api_covid/public/postDHistory";     
        
        $params['headers'] = $this->myHeader;
        $params['form_params'] = [            
            'id' => $id,
        ];           

        $response = $this->Client->post($url, $params);        
        $history = \GuzzleHttp\json_decode($response->getBody(), true);
        
        return redirect($_SERVER['HTTP_REFERER']);
    }
}    