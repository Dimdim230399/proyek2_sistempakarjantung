<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class infoCovController extends Controller
{
    public function indexCov(){
        $request = Http::get('https://indonesia-covid-19.mathdro.id/api/provinsi');
        $data = $request->json();
        // dd($data);
        return view('index_user',compact('data'));
    }
}
