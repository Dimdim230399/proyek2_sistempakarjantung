<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Charts\CovidChart;
use ApiHelper;
use Session;

class userController extends Controller
{

    public function userhal(){
        $request = collect( Http::get('https://covid19.keponet.com/api/provinsi/')->json());    
        $request2 = collect( Http::get('https://covid19.keponet.com/api/negara/')->json());
        $request3 = collect( Http::get('http://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=8a77f5f25eae40668093f1dab97a80a0')->json());
        $data  = $request->flatten(1);
        $data1 = $request2->flatten(1);
        // $data2 = $request3->flatten(2);

        $labels = $data->pluck('Provinsi');
        $positif = $data->pluck('Kasus_Posi');
        $recov = $data->pluck('Kasus_Semb');
        $deat = $data->pluck('Kasus_Meni');

        // $chart = new CovidChart;
        // // $chart->labels($labels);
        // // $chart->dataset('Data Kasus Positif Covid-19' , 'pie', $positif);
        //  dd($request3);
        return view('index_user',compact('data1','labels','positif','recov','deat','request3'));
    }
    public function index()
    {
        return view('soal/identitas');
    }

    public function getSoal()
    {
        $client = ApiHelper::loadfile();

        $url ="http://127.0.0.1/api_covid/public/getSoal";
        $response = $client->get($url);#Send request by http method=>GET

        $dataPangkat=\GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET
        $data = $dataPangkat['result'];

        return view('soal/soal', compact('data'));

    }

    public function postBiodata(Request $request)
    { 
        $request->except('_token');

        $client = ApiHelper::loadfile();

        $url = "http://127.0.0.1/api_covid/public/postDataDiri";    
        
        $params['form_params'] = $request->except('_token');
        $response = $client->post($url, $params);
        $dataRecent=\GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET
        session::put('user_pasien', $dataRecent);        
        return redirect()->route('soal');
    }
    
    public function postJawaban(Request $request)
    { 
        $request->except('_token');

        $client = ApiHelper::loadfile();

        $url = "http://127.0.0.1/api_covid/public/postJawaban";    
        
        $params['form_params'] = $request->except('_token');
        $response = $client->post($url, $params);
        $dataRecent=\GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET
        
    }

    public function getHasil($id_history)
    {
        $client = ApiHelper::loadfile();

        $url ="http://127.0.0.1/api_covid/public/getHasil/".$id_history;
        $response = $client->get($url);#Send request by http method=>GET

        $dataPangkat=\GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET

        return response()->json($dataPangkat);
    }

    public function getPerawatan($id_penyakit)
    {
        $client = ApiHelper::loadfile();

        $url ="http://127.0.0.1/api_covid/public/getHasil/perawatan/".$id_penyakit;
        $response = $client->get($url);#Send request by http method=>GET

        $dataPerawatan=\GuzzleHttp\json_decode($response->getBody(), true);#Receive request by http method=>GET

        return response()->json($dataPerawatan);
    }
}
