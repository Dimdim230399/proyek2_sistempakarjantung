<!-- Footer -->
<footer class="footer bg-white">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item" style="color: black;">
                    2020 | made with <i class="fa fa-heart heart text-danger"></i> by Us
                </li>
            </ul>
        </nav>
    </div>
</footer>