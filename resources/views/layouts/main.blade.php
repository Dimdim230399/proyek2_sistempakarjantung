<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
    <style>
        .centerr {
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%)
        }
    </style>
</head>

<body>
    <div class="wrapper">
    
        @include('layouts.navbar')
        @include('layouts.sidebar')

        <!-- Main Content -->
        <div class="main-panel">

            <!-- Content -->
            <div class="content">
                @yield('content')
            </div>

            @include('layouts.footer')            
        </div>

    </div>
    @include('layouts.script')

</body>

</html>