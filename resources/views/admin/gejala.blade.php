@extends('layouts.main')

@php
$data_session = Session::get('user_data');
$token = $data_session['token'];
$namaUser = $data_session['name'];
$url = url()->current();
@endphp

@section('content')

<div class="panel-heading bg-primary">
    <div class="page-inner ">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pt-2 pb-2 fw-bold">Selamat Datang</h2>
            </div>
        </div>
    </div>
</div>
<div class="panel-body bg-light">
    <div class="page-inner pb-0 pr-0 pl-0">

        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-12">
                    <div class="card">
                        {{-- atas --}}
                        <div class="card-header">
                            <div class="card-head-row card-tools-still-right">
                                <h4 class="card-title">Data Gejala</h4>
                                <div class="card-tools">
                                    <button class="btn btn-success btn-round ml-auto btnTambah" data-toggle="modal"
                                        data-target="#modalGejala"><i class="fa fa-plus"></i>&nbsp Tambah Data</button>
                                </div>
                            </div>
                        </div>

                        {{-- isi table --}}
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_gejala" class="display table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="2%">No</th>
                                            <th width="25%">Nama Gejala</th>
                                            <th width="2%" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalGejala" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formGejala" name="formGejala">
                    @csrf
                    <input id="id" hidden type="text" name="id" class="form-control" value="">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-control-label mb-0" for="nama_gejala">Nama Gejala</label>
                            <input id="nama_gejala" name="nama_gejala" type="text" class="form-control"
                                placeholder="Nama gejala...">
                        </div>
                    </div>
            </div>
            <div class="modal-footer no-bd">
                <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    var table = $('#table_gejala').DataTable({
        "ordering": true,
        "order": [
            [2, 'ASC']
        ],
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax: "{{ route('gejala') }}",
        columns: [{
            data: "id",
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: 'nama_gejala',
            name: 'nama_gejala',
            orderable: true
        }, {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
        }, ]
    });

    $(document).on('click', '.editData', function () {
        $('#title').html('Ubah Data Gejala');
        $('#id').val($(this).data('id'));
        $('#nama_gejala').val($(this).data('nama_gejala'));
        $('#modalGejala').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        })
    });

    $(document).ready(function () {
        $('.btnTambah').click(function () {
            $('#title').html('Tambah Data Gejala');
            $('#modalGejala').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
        });
    });

    $('#btnSave').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formGejala').serialize(),
            url: "{{ route('simpan_gejala') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formGejala').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#modalGejala').modal('hide');
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
            }
        })
    });

    $(document).on('click', '.btnDelete', function (e) {

        var id = $(this).data('id');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Yes, delete it!',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $.ajax({
                    url: '{{ $url }}/' + id,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses hapus data: ', id);
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });
                swal({
                    title: 'Deleted!',
                    text: 'Your data has been deleted!',
                    type: 'success',
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    }
                });
            } else {
                swal.close();
            }
        });
    });
</script>
@endsection