@extends('layouts.main')

@php
$data_session = Session::get('user_data');
$token = $data_session['token'];
$namaUser = $data_session['name'];
$url = url()->current();
@endphp

@section('content')

<div class="panel-heading bg-primary">
    <div class="page-inner ">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">Selamat Datang</h2>
            </div>
        </div>
    </div>
</div>

<div class="panel-body bg-light">
    <div class="page-inner pb-0 pr-0 pl-0">
        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-12">
                    <div class="card">
                        {{-- atas --}}
                        <div class="card-header">
                            <div class="card-head-row card-tools-still-right">
                                <h4 class="card-title">Data Pasien</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="pasienTable" class="display table table-bordered table-hover">
                                    <thead>
                                        <tr class="text-center">
                                            <th width="2%" class="text-center">No.</th>
                                            <th width="30%" class="text-center">Nama</th>
                                            <th width="10%" class="text-center">Usia</th>
                                            <th width="12%" class="text-center">TB</th>
                                            <th width="10%" class="text-center">BB</th>
                                            <th width="25%" class="text-center">Telepon</th>
                                            <th width="5%" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table = $('#pasienTable').DataTable({
        "ordering": true,
        "order": [
            [1, 'ASC']
        ],
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax: "{{ route('pasien') }}",
        columns: [{
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                "data": "nama"
            },
            {
                "data": "usia"
            },
            {
                "data": "tinggi_badan"
            },
            {
                "data": "berat_badan"
            },
            {
                "data": "hp"
            },
            {
                "data": "action",
                orderable: false,
                searchable: false
            },
        ]
    });

    $('body').on('click', '.btnDelete', function () {

        var id_pasien = $(this).data('id');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Yes, delete it!',
                    className: 'btn btn-success'

                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                console.log($(this).data('id'));
                $.ajax({
                    url: '{{ $url }}/' + id_pasien,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });
                swal({
                    title: 'Deleted!',
                    text: 'Your file has been deleted.',
                    type: 'success',
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    }
                });
            } else {
                swal.close();
            }
        });
    });
</script>

@endsection