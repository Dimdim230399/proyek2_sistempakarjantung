@extends('layout_user.mainlogin')


@section('content')

<div class="page-header header-filter"
    style="background-image: url('{{asset('asset_user/img/cov4.gif')}}'); background-size: cover; background-position: top center;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                <div class="card card-login">
                    <form class="form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="card-header card-header-primary text-center">
                            <h3 class="card-title">L O G I N</h3>
                            <div class="social-line">
                                <a class="btn btn-just-icon btn-link btn-lg">
                                    <i class="material-icons">phone_enabled</i>
                                </a>
                                <a class="btn btn-just-icon btn-link btn-lg">
                                    <i class="material-icons">coronavirus</i>
                                </a>
                                <a class="btn btn-just-icon btn-link btn-lg">
                                    <i class="material-icons">rss_feed</i>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">person</i>
                                    </span>
                                </div>
                                <input type="text" id="username"
                                    class="form-control @error('username') is-invalid @enderror" name="username"
                                    value="{{ old('username') }}" required autocomplete="username"
                                    placeholder="Username">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">vpn_key</i>
                                    </span>
                                </div>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password" placeholder="Password">
                                
                            </div>
                        </div>
                        <br><br>
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-primary btn-round"
                            style="color: white;">L o g i n</button>
                        </div>
                        <div class="footer text-center">
                            Belum punya akun? <a href="{{ route('Reg_user') }}">Registrasi</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- <script>
    $(document).ready(function () {
    
        $('#btlogin').click(function (e) {
            e.preventDefault();
            $(this).html('Sik Di check...');
                $.ajax({
                    data: $('#formLogin').serialize(),
                    url: "{{ route('login') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $('#formLogin').trigger('reset');
                        // toastr.success(data.success);
                        // location.reload(true);
                        },
                    error: function (data) {
                        console.log('Error:', data);
                        }
                });
        });
    }); --}}
</script>


@endsection

