@extends('layout_user.mainlogin')


@section('content')
<div class="page-header header-filter"
    style="background-image: url('{{asset('asset_user/img/cov4.gif')}}'); background-size: cover; background-position: top center;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                <div class="card card-login">
                    <form class="form" method="POST" action="{{route('kirim_register')}}">
                        @csrf
                        <div class="card-header card-header-primary text-center">
                            <h3 class="card-title">REGISTRASI</h3>
                            <div class="social-line">
                                <a class="btn btn-just-icon btn-link btn-lg">
                                    <i class="material-icons">phone_enabled</i>
                                </a>
                                <a class="btn btn-just-icon btn-link btn-lg">
                                    <i class="material-icons">coronavirus</i>
                                </a>
                                <a class="btn btn-just-icon btn-link btn-lg">
                                    <i class="material-icons">rss_feed</i>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <input id="level" type="hidden" value="user" class="form-control" name="level">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">face</i>
                                    </span>
                                </div>
                                <input type="text" id="name" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                    placeholder="Nama">
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">person</i>
                                    </span>
                                </div>
                                <input type="text" id="username"
                                    class="form-control @error('username') is-invalid @enderror" name="username"
                                    value="{{ old('username') }}" required autocomplete="username" autofocus
                                    placeholder="Username">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">mail_outline</i>
                                    </span>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email"
                                    placeholder="Example@mail.com">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">vpn_key</i>
                                    </span>
                                </div>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password" placeholder="Password">
                            </div>

                        </div>
                        <br>                        
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-primary btn-round"
                            style="color: white;">R e g i s t r a s i</button>
                        </div>
                        <div class="footer text-center">
                            Sudah punya akun? <a href="{{ url('/login') }}">Login</a>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var textfield = $("input[name=password]");

    if (textfield.val() == "")
        $(".eye").hide();

    $("#password").on("keyup", function () {
        if (textfield.val() == "")
            $(".eye").hide();
        else
            $(".eye").show();
    });

    function change() {
        var x = document.getElementById('password').type;

        if (x == 'password') {
            document.getElementById('password').type = 'text';
            document.getElementById('eyeButton').innerHTML = '<i class="material-icons eye">visibility_off</i>';
        } else {
            document.getElementById('password').type = 'password';
            document.getElementById('eyeButton').innerHTML = '<i class="material-icons eye">visibility</i>';
        }
    }
</script>
@endsection
