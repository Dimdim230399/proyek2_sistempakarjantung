@extends('layout_user.main')
@section('content')



<div class="page-header header-filter" data-parallax="true"
    style="background-image: url('{{asset('asset_user/img/cov4.gif')}}')">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="title">Covid-19</h1>
                <h4>Hadir untuk membantu Anda memantau kondisi kesehatan<br>Anda.
                    Untuk sekarang dan masa yang akan datang,
                    untuk Anda dan generasi selanjutnya.</h4>
                <br>
                <a href="#pengertian" class="btn btn-success btn-raised btn-md">Mulai
                </a>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-center" id="pengertian">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    <h2 class="title">Apa itu Covid-19?</h2>
                    <h4 class="description"><strong>Virus Corona atau <i>Severe Acute Respiratory Syndrome
                        Coronavirus 2</i> (SARS-CoV-2) adalah virus yang menyerang sistem pernapasan.
                        Penyakit karena infeksi virus ini disebut Covid-19. Virus Corona bisa menyebabkan
                        gangguan ringan pada sistem pernapasan, infeksi paru-paru yang berat, hingga kematian.
                        <br>
                        Infeksi Virus Corona (Corona Virus Disease 2019/Covid-19) pertama kali ditemukan di
                        Kota Wuhan, China pada akhir Desember 2019. Virus ini menular dengan sangat cepat dan telah
                        menyebar ke hampir semua negara, termasuk Indonesia, hanya dalam waktu beberapa bulan.
                    .</strong></h4>
                </div>
            </div>

            <br>

            <div class="features">
                <div class="row">
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-info">
                                <i class="fas fa-vial"></i>
                            </div>
                            <h3 class="info-title">10+</h3>
                            <h5>Gejala dan kondisi kesehatan teridentifikasi.</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-success">
                                <i class="fas fa-check-circle"></i>
                            </div>
                            <h3 class="info-title">90%</h3>
                            <h5>Hasil yang ditampilkan mendekati kebenaran.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-danger">
                                <i class="fas fa-medkit"></i>
                            </div>
                            <h3 class="info-title">50+</h3>
                            <h5>Cara perawatan terhadap hasil yang anda dapatkan.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="#tentang" class="btn btn-success btn-raised btn-md">Selanjutnya
                </a>
            </div>
        </div>

        <hr>

        <div class="section text-center" id="tentang">
            <h2 class="title">Tentang</h2>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-text card-header-danger">
                            <div class="card-text">
                                <h3 class="card-title">Apa<br>penyebabnya?</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="float-left">Usia</li><br>
                                <li class="float-left">Obesitas</li><br>
                                <li class="float-left">Kurang olahraga</li><br>
                                <li class="float-left">Alkohol</li><br>
                                <li class="float-left">Keturunan</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-text card-header-warning">
                            <div class="card-text">
                                <h3 class="card-title">Apa tanda dan gejalanya?</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="float-left">Nyeri dada</li><br>
                                <li class="float-left">Mudah lelah</li><br>
                                <li class="float-left">Jantung berdebar-debar</li><br>
                                <li class="float-left">Gelisah, cemas, dan mudah lelah</li><br>
                                <li class="float-left">Sesak nafas saat beraktifitas</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-text card-header-success">
                            <div class="card-text">
                                <h3 class="card-title">Bagaimana mencegahnya?</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="float-left">Rutin cek kolesterol dan tekanan darah</li><br>
                                <li class="float-left">Pola Hidup Sehat</li><br>
                                <li class="float-left">Tidak merokok dan tidak minum alkohol</li><br>
                                <li class="float-left">Menjaga berat badan tubuh tetap ideal</li><br>
                                <li class="float-left">Istirahat yang cukup</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><br>

            <div class="title">
                <h3>Persebaran Covid-19</h3>
            </div>
            <div class="card card-raised card-carousel">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{asset('asset_user/img/Covid-19_spread_timeline.gif')}}"
                                alt="First slide">                            
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <i class="material-icons">keyboard_arrow_left</i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <i class="material-icons">keyboard_arrow_right</i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <br>
            <br>

            <div class="card">
                <div class="card-header card-header-text card-header-success">
                    <div class="card-text">
                        <h3 class="card-title">Monitoring Persebaran Covid-19 di Indonesia</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chardData"></div>
                   <script>
                        Highcharts.chart('chardData', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Monitoring Covid-19'
                            },
                            subtitle: {
                                text: 'Data Persebaran Covid-19 Tiap Provinsi'
                            },
                            xAxis: {
                                categories: {!!json_encode($labels)!!},
                                crosshair: true
                            },
                            yAxis: {
                                min: 100,
                                title: {
                                    text: 'Jumlah Orang'
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y:.1f} orang</b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: [{
                                name: 'Positif',
                                data: {!!json_encode($positif)!!}

                            },{
                                name: 'Sembuh',
                                data: {!!json_encode($recov)!!}

                            },{
                                name: 'Meninggal',
                                data: {!!json_encode($deat)!!}

                            }]
                        });
                    </script>

                </div>
            </div>

            <br>
            <br>
            <div class="card">
                <div class="card-header card-header-text card-header-success">
                    <div class="card-text">
                        <h3 class="card-title">Monitoring Persebaran Covid-19 Tiap Negara</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datainfo" class="display table-hover table-bordered" style="width:100%">
                        <thead class="">
                            <tr>
                                <th>No</th>
                                <th>Negara</th>
                                <th>Kasus Positif</th>
                                <th>Kasus Sembuh</th>
                                <th>Kasus Meninggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($data1 as $datacov)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$datacov['Country_Region']}}</td>
                                <td>{{$datacov['Confirmed']}}</td>
                                <td> @if($datacov['Recovered']==null or "")
                                    {{"0"}}
                                @else
                                    {{$datacov['Recovered']}}
                                @endif
                                </td>
                                <td>{{$datacov['Deaths']}}</td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
        <hr>

        <div class="section" id="berita">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto ">
                    <h2 class="text-center title">News</h2>
                    <div class="row slider-for">
                    @foreach ($request3['articles'] as $item)
                        <div class="col-md-6">
                            <div class="page-header header container1" style="height:400px;background-image: url('{{$item['urlToImage']}}')">
                                <div class="overlay1">
                                    <div class="text1 row">
                                        <div class="col-md-12">
                                            <p style="font-size: 24px">
                                                {{$item['description']}}<h3><a href="{{$item['url']}}" target="blank" style="text-decoration: none; color : blanchedalmond ; text-transform: bold" > Baca Selengkapnya....</a></h3>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="container judulBerita">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <h3 class="title mb-0 bg-danger">{{$item['title']}}</h3>
                                            <h5 class=" bg-dark mt-0"> {{ApiHelper::format_tanggal($item['publishedAt'])}}, {{$item['author']}}</h5>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                    <br>
                    <div class="row slider-nav">
                    @foreach ($request3['articles'] as $item)
                        <div class="col-md-6">
                            <div class="page-header header-filter " style="height:150px;background-image: url('{{$item['urlToImage']}}')">

                                <div class="container ">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="title mb-0">{{$item['title']}}</h6>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                    <br>
                    <div class="col-md-4 ml-auto mr-auto text-center">
                        <a href="{{ route('biodata') }}" class="btn btn-success btn-raised btn-md">Periksa</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal Edit user --}}
<!-- Classic Modal -->
<!-- <div class="modal fade" id="editprofil" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Akun</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group bmd-form-group">
                    <label for="exampleInput1" class="bmd-label-floating">Nama User</label>
                    {{-- <input type="text" class="form-control" value="{{$namaUser}}" id="exampleInput1"> --}}
                    <span class="bmd-help">Nama Lengkap Anda</span>
                </div>

                <div class="form-group bmd-form-group">
                    <label for="exampleInput1" class="bmd-label-floating">UserName</label>
                    {{-- <input type="text" class="form-control" value="{{$username}}" id="exampleInput1"> --}}
                    <span class="bmd-help">UserName Untuk Login</span>
                </div>

                <div class="form-group bmd-form-group">
                    <label for="exampleInput1" class="bmd-label-floating">Password</label>
                    <input type="password" class="form-control" id="exampleInput1">
                    <span class="bmd-help">Password Akun Anda</span>
                </div>

                <div class="form-group bmd-form-group">
                    <label for="exampleInput1" class="bmd-label-floating">Email Anda</label>
                    {{-- <input type="email" class="form-control" value="{{$email}}" id="exampleInput1"> --}}
                    <span class="bmd-help">Email Akun Anda</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-link">Simpan</button>
            </div>
        </div>
    </div>
</div> -->
<!--  End Modal -->
<script>
$(document).ready(function(){

    $('.container1').mouseenter(function (e) {
        $('.judulBerita').attr('style','display:none');
    }).mouseleave(function (e) {
        $('.judulBerita').removeAttr('style','display:none');
    })

        var table = $('#datainfo').DataTable({
            "ordering": true,
            "order": [
                [2, 'ASC']
            ],
            processing: true,
            serverSide: false,
            "scrollY": "500px",
            "scrollCollapse": true,
        });

    $('.slider-for').slick({
    arrows: false,
    asNavFor: '.slider-nav,.slider-x',
    });

    $('.slider-nav').slick({
    arrows: true,
    asNavFor: '.slider-for,.slider-x',
    centerMode: true,
    centerPadding: '50px',
    dots: true,
    focusOnSelect: true,
    slidesToShow: 5,
     autoplay: true,
     autoplaySpeed: 2000,
       responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
    });

    // Initialize Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Add smooth scrolling to all links in navbar + footer link
    $('a[href^="#"]').on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior


        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 900, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    })

});
</script>
@endsection
