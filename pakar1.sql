-- Adminer 4.7.7 PostgreSQL dump

\connect "pakar1";

DROP TABLE IF EXISTS "history";
DROP SEQUENCE IF EXISTS history_id_seq;
CREATE SEQUENCE history_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."history" (
    "id_history" integer DEFAULT nextval('history_id_seq') NOT NULL,
    "id_pasien" integer,
    "created_at" date DEFAULT now()
) WITH (oids = false);

INSERT INTO "history" ("id_history", "id_pasien", "created_at") VALUES
(11,	12,	'2020-09-01');

DROP TABLE IF EXISTS "history_gejala";
DROP SEQUENCE IF EXISTS history_gejala_id_seq;
CREATE SEQUENCE history_gejala_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."history_gejala" (
    "id" integer DEFAULT nextval('history_gejala_id_seq') NOT NULL,
    "kode_gejala" character varying NOT NULL,
    "id_history" integer NOT NULL,
    CONSTRAINT "history_gejala_id" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "history_gejala" ("id", "kode_gejala", "id_history") VALUES
(31,	'G06',	4),
(32,	'G16',	4),
(33,	'G02',	5),
(34,	'G04',	5),
(35,	'G07',	5),
(36,	'G06',	5),
(37,	'G08',	5),
(38,	'G09',	5),
(39,	'G12',	5),
(40,	'G14',	5),
(41,	'G19',	5),
(42,	'G21',	5),
(43,	'G23',	5),
(44,	'G25',	5),
(45,	'G26',	5),
(46,	'G02',	6),
(47,	'G04',	6),
(48,	'G06',	6),
(49,	'G08',	6),
(50,	'G10',	6),
(51,	'G11',	6),
(52,	'G13',	6),
(53,	'G14',	6),
(54,	'G18',	6),
(55,	'G16',	6),
(56,	'G17',	6),
(57,	'G20',	6),
(58,	'G19',	6),
(59,	'G23',	6),
(61,	'G27',	6),
(60,	'G24',	6),
(62,	'G26',	6),
(63,	'G25',	6),
(64,	'G25',	6),
(65,	'G01',	7),
(66,	'G02',	7),
(67,	'G03',	7),
(68,	'G05',	7),
(69,	'G08',	7),
(70,	'G09',	7),
(71,	'G11',	7),
(72,	'G13',	7),
(73,	'G10',	7),
(74,	'G14',	7),
(75,	'G16',	7),
(76,	'G15',	7),
(77,	'G23',	7),
(78,	'G21',	7),
(79,	'G22',	7),
(80,	'G24',	7),
(81,	'G25',	7),
(82,	'G27',	7),
(83,	'G02',	11),
(84,	'G03',	11),
(85,	'G04',	11),
(86,	'G06',	11),
(87,	'G07',	11),
(88,	'G08',	11),
(89,	'G10',	11),
(90,	'G11',	11),
(91,	'G14',	11),
(92,	'G16',	11),
(93,	'G18',	11),
(94,	'G19',	11),
(95,	'G21',	11),
(96,	'G20',	11),
(97,	'G22',	11),
(98,	'G24',	11),
(99,	'G25',	11),
(100,	'G25',	11),
(101,	'G26',	11);

DROP TABLE IF EXISTS "history_penyakit";
DROP SEQUENCE IF EXISTS history_penyakit_id_seq;
CREATE SEQUENCE history_penyakit_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."history_penyakit" (
    "id" integer DEFAULT nextval('history_penyakit_id_seq') NOT NULL,
    "kode_gejala" character varying,
    "kode_penyakit" character varying
) WITH (oids = false);


DROP TABLE IF EXISTS "m_gejala";
DROP SEQUENCE IF EXISTS gejala_id_seq;
CREATE SEQUENCE gejala_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."m_gejala" (
    "id_gejala" integer DEFAULT nextval('gejala_id_seq'),
    "nama_gejala" character varying(250),
    "created_at" timestamp,
    "created_by" character varying,
    "updated_by" character varying,
    "updated_at" timestamp,
    "kode_gejala" character varying(100)
) WITH (oids = false);

INSERT INTO "m_gejala" ("id_gejala", "nama_gejala", "created_at", "created_by", "updated_by", "updated_at", "kode_gejala") VALUES
(1,	'nyeri dada',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G01'),
(2,	'perasaan tertekan /dada serasa di remas',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G02'),
(3,	'sesak nafas',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G03'),
(4,	'mual',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G04'),
(5,	'gangguan pencernaan',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G05'),
(6,	' keringat dingin',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G06'),
(7,	' sudah di rasa hampir  1 tahun',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G07'),
(8,	'Pusing',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G08'),
(9,	'jantung berdebar / berdetang kencang',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G09'),
(10,	'denyut nadi lambat',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G10'),
(11,	'sering pingsan',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G11'),
(12,	'kulit berwarna biru',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G12'),
(13,	'pembengkakan di kaki',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G13'),
(14,	'kelelahan',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G14'),
(15,	'irama jantung tidak teratur',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G15'),
(16,	'Batuk terus menerus',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G16'),
(17,	'Kebutuhan buang air kecil meningkat',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G17'),
(18,	'pembengkakan perut',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G18'),
(19,	'Berat badan meningkat sangat cepat',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G19'),
(20,	'Tidak nafsu makan',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G20'),
(21,	'Sulit konsentrasi / penurunan kewaspadaan',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G21'),
(22,	'Kembung',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G22'),
(23,	'Demam',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G23'),
(24,	'Menggigil',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G24'),
(25,	'Keringat Berlebih',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G25'),
(26,	'Tekanan Darah Rendah',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G26'),
(27,	'Penurunan Berat Badan',	'2020-08-18 12:36:00.74',	'admin',	NULL,	NULL,	'G27'),
(1,	'Susah Nafas',	'2020-08-31 09:26:01',	'ADMIN',	NULL,	'2020-08-31 09:26:01',	'G25');

DROP TABLE IF EXISTS "m_penyakit";
DROP SEQUENCE IF EXISTS penyakit_id_seq;
CREATE SEQUENCE penyakit_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."m_penyakit" (
    "id" integer DEFAULT nextval('penyakit_id_seq') NOT NULL,
    "kode_penyakit" character varying(100),
    "nama_penyakit" character varying(100),
    "deskripsi_penyakit" text,
    "created_at" timestamp,
    "updated_at" timestamp,
    "updated_by" character varying(100),
    "created_by" character varying(100)
) WITH (oids = false);

INSERT INTO "m_penyakit" ("id", "kode_penyakit", "nama_penyakit", "deskripsi_penyakit", "created_at", "updated_at", "updated_by", "created_by") VALUES
(1,	'P01',	'Penyakit jantung coroner',	'Jantung Koroner merupakan jenis penyakit jantung pertama yang umum terjadi. Penyakit Jantung Koroner (PJK) atau arteri koroner terjadi ketika arteri di jantung menyempit atau tersumbat',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(2,	'P02',	'serangan jantung',	'Serangan jantung merupakan kondisi darurat yang terjadi saat pasokan darah ke jantung terhambat secara total, sehingga sel-sel otot jantung mengalami kerusakan. Serangan jantung biasanya disebabkan oleh penyakit jantung koroner.',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(3,	'P03',	'aritma',	'Aritmia merupakan irama jantung yang abnormal. Saat seseorang mengalami aritmia, jantung memiliki pola detak yang tidak teratur. Aritmia serius sering berkembang dari masalah jantung lain tetapi juga bisa terjadi sendiri. Detak jantung yang tidak teratur adalah umum dan semua orang mengalaminya.Namun, ketika detak jantung berubah terlalu banyak atau terjadi karena jantung yang rusak atau lemah',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(4,	'P04',	'Penyakit jantung bawaan',	'Penyakit jantung bawaan merupakan istilah umum untuk beberapa kelainan bentuk jantung yang telah ada sejak lahir. Contoh penyakit jantung bawaan di antaranya adalah:
1.Cacat septum: Adanya lubang antara dua bilik jantung. Kelainan obstruksi: Aliran darah melalui berbagai bilik jantung sebagian atau seluruhnya tersumbat.                                     2.Penyakit jantung sianotik: Kerusakan pada jantung menyebabkan kekurangan oksigen di seluruh tubuh.',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(5,	'P05',	'Gagal Jantung',	'Gagal jantung juga dikenal sebagai gagal jantung kongestif. Jenis penyakit jantung terjadi ketika jantung tidak memompa darah ke seluruh tubuh secara efisien. Penyakit arteri koroner atau tekanan darah tinggi dapat, dari waktu ke waktu, membuat jantung terlalu kaku atau lemah untuk diisi dan dipompa dengan baik',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(6,	'P06',	'Penyakit Katup Jantung',	'Jantung memiliki empat katup yang membuka dan menutup aliran darah langsung antara empat ruang jantung, paru-paru, dan pembuluh darah. Cacat bisa membuat katup sulit untuk membuka dan menutup dengan benar. Ketika itu terjadi, aliran darah bisa tersumbat atau darah bisa bocor.

Penyebab masalah katup jantung termasuk infeksi seperti demam rematik, penyakit jantung bawaan, tekanan darah tinggi, penyakit arteri koroner, atau akibat serangan jantung',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(7,	'P07',	'Penyakit Otot Jantung',	'Kardiomiopati adalah jenis penyakit jantung yang menyebabkan otot-otot jantung tumbuh lebih besar dan menjadi kaku, tebal, atau lemah. Jantung mungkin terlalu lemah untuk memompa dengan baik.',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(8,	'P08',	'Endokarditis',	'Jenis penyakit Endokarditis merupakan infeksi pada jaringan ikat yang melapisi dinding dan katup jantung. Infeksi ini terjadi ketika kuman dari bagian tubuh lain seperti mulut dan kulit, masuk ke dinding jantung melalui aliran darah',	'2020-08-18 12:39:15.18',	NULL,	NULL,	'admin'),
(9,	'P09',	'Tumor Jantung',	'Tumor jantung merupakan pertumbuhan jaringan abnormal padad dinding jantung. Tumor dapat bersifat kanker (ganas) atau non-kanker (jinak). Tumor ini dapat tumbuh di dinding otot jantung atau lapisan pelindung jantung (perikardium)',	NULL,	NULL,	NULL,	NULL),
(1,	'P10',	'Jantung  Rusak',	'Mohon Maaf Jantung Anda Tidak dapat Di obati',	'2020-08-31 09:25:01',	'2020-08-31 09:25:01',	NULL,	'ADMIN');

DROP TABLE IF EXISTS "m_perawatan";
DROP SEQUENCE IF EXISTS perawatan_id_seq;
CREATE SEQUENCE perawatan_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."m_perawatan" (
    "id" integer DEFAULT nextval('perawatan_id_seq') NOT NULL,
    "id_penyakit" integer,
    "deskripsi_perawatan" text,
    "created_at" timestamp,
    "created_by" character varying(100),
    "updated_at" timestamp NOT NULL,
    "updated_by" character varying(100) NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "mapping_gejala_penyakit";
DROP SEQUENCE IF EXISTS mapping_gejala_penyakit_id_seq;
CREATE SEQUENCE mapping_gejala_penyakit_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."mapping_gejala_penyakit" (
    "id_mapping" integer DEFAULT nextval('mapping_gejala_penyakit_id_seq') NOT NULL,
    "kode_gejala" character varying,
    "kode_penyakit" character varying(32),
    "created_at" timestamp,
    "created_by" character varying(100),
    "updated_at" timestamp,
    "updated_by" character varying(100),
    "deskripsi_perawatan" text
) WITH (oids = false);

INSERT INTO "mapping_gejala_penyakit" ("id_mapping", "kode_gejala", "kode_penyakit", "created_at", "created_by", "updated_at", "updated_by", "deskripsi_perawatan") VALUES
(56,	'G02',	'P01',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(57,	'G03',	'P01',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(58,	'G04',	'P01',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(59,	'G05',	'P01',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(60,	'G01',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(61,	'G02',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(62,	'G03',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(63,	'G04',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(64,	'G05',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(65,	'G06',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(66,	'G07',	'P02',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(67,	'G01',	'P03',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(68,	'G08',	'P03',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(69,	'G09',	'P03',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(70,	'G10',	'P03',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(71,	'G11',	'P03',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(72,	'G03',	'P04',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(73,	'G12',	'P04',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(74,	'G13',	'P04',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(75,	'G14',	'P04',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(76,	'G15',	'P04',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(77,	'G13',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(78,	'G14',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(79,	'G15',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(80,	'G16',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(81,	'G17',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(82,	'G18',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(83,	'G19',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(84,	'G20',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(85,	'G21',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(86,	'G08',	'P06',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(87,	'G11',	'P06',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(88,	'G03',	'P06',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(89,	'G13',	'P06',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(90,	'G14',	'P06',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(91,	'G15',	'P06',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(92,	'G03',	'P07',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(93,	'G09',	'P07',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(94,	'G13',	'P07',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(95,	'G14',	'P07',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(96,	'G22',	'P07',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(97,	'G03',	'P08',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(98,	'G22',	'P08',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(99,	'G23',	'P08',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(100,	'G24',	'P08',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(101,	'G25',	'P08',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(102,	'G03',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(103,	'G08',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(104,	'G09',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(105,	'G13',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(106,	'G14',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(107,	'G26',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(108,	'G27',	'P09',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	NULL),
(109,	'G01',	'P01',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	'BEROBAT dong!'),
(110,	'G09',	'P05',	'2020-08-18 12:44:44.33',	'admin',	NULL,	NULL,	'BEROBAT dong!');

DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "pasien";
DROP SEQUENCE IF EXISTS pasien_id_seq;
CREATE SEQUENCE pasien_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."pasien" (
    "id" integer DEFAULT nextval('pasien_id_seq') NOT NULL,
    "hp" character varying(13),
    "nama" character varying(50),
    "usia" integer,
    "tinggi_badan" smallint,
    "berat_badan" smallint,
    "created_at" timestamp NOT NULL,
    "updated_at" timestamp NOT NULL,
    CONSTRAINT "pasien_id" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "pasien" ("id", "hp", "nama", "usia", "tinggi_badan", "berat_badan", "created_at", "updated_at") VALUES
(7,	'0739462354',	'Dimas',	21,	170,	65,	'2020-08-31 09:32:31',	'2020-08-31 09:32:31');

DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "email" character varying(255) NOT NULL,
    "email_verified_at" timestamp(0),
    "password" character varying(255) NOT NULL,
    "token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "level" character varying(15) DEFAULT 'admin',
    "username" character varying(50) NOT NULL,
    CONSTRAINT "users_email_unique" UNIQUE ("email"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "users" ("id", "name", "email", "email_verified_at", "password", "token", "created_at", "updated_at", "level", "username") VALUES
(11,	'ADMIN',	'admin@gmail.com',	NULL,	'$2y$10$BW5iwdhjvqFsGN9DWGed/uWBY34u.OH8lm4UB3JjJixC8nUSxwyu2',	'',	'2020-08-18 11:35:12',	'2020-09-04 23:59:56',	'admin',	'admin');

-- 2020-09-05 07:44:05.111022+07
